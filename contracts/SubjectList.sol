// SPDX-License-Identifier:MIT
pragma solidity ^0.8.0;

contract SubjectList{
    uint public subjectsCount = 0;

    //model a subject
    struct Subject{
        uint _id;
        string code;
        string name;
        bool retired;
    }
    //store subjects
    mapping(uint => Subject) public subjects;

    // //Constructor for students
    // constructor() {
    //     createSubject(1001, "Dapp");
    // }

    //Events
    event createSubjectEvent (
        uint _id,
        string code,
        string name,
        bool retired
    );


    //create and add subeject to storage
    function createSubject(string memory _code, string memory _name)

    public returns (Subject memory) { 
        subjectsCount++; //increments the student count
        subjects[subjectsCount] = Subject(subjectsCount, _code, _name, false);

        //trigger create event
        emit createSubjectEvent(subjectsCount, _code, _name, false);
        return subjects[subjectsCount]; // maps student to students
    }

    //event for retirede status
    event markRetiredEvent(
        uint indexed code
    );

    //change the retired status of the student
    function markRetired(uint _id) public returns (Subject memory){
        subjects[_id].retired=true;

        //trigger create event
        emit markRetiredEvent(_id);
        return subjects[_id];
    }
    //Fetch Subject
    function findSubject(uint _id) public view returns(Subject memory){
        return subjects[_id];
    }

    
    
}

// SPDX-License-Identifier:MIT
pragma solidity ^0.8.0;

contract StudentList {
    uint public studentsCount = 0;  //state variable

    //model a student
    struct Student {
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }
    mapping(uint => Student) public students; // map student with student ID (for storage)

    // constructor for students
    constructor() {
        createStudent(1001, "Tshewang Penjor Wangchuk");
    }

    // Events
    event createStudentEvent(
        uint _id,
        uint indexed cid,
        string name,
        bool graduated
    ); // indexing based in cid

    //create and add student to storage
    function createStudent(uint _studentCid, string memory _name)  ///store name in memory instead of storage(temporary)
    public returns (Student memory) { 
        studentsCount++; //increments the student count
        students[studentsCount] = Student(studentsCount, _studentCid, _name, false);

        //trigger create event
        emit createStudentEvent(studentsCount, _studentCid, _name, false);
        return students[studentsCount]; // maps student to students
    }

    // event for graduation sataus
    event markGraduatedEvent(
        uint indexed cid
        );
    
    //change graduation status of the student
    function markGraduated(uint _id) public returns (Student memory) {
        students[_id].graduated = true;
        //trigger create evevnt
        emit markGraduatedEvent(_id);
        return students[_id];
    }

    //fetch student info from stroage
    function findStudent(uint _id) public view returns (Student memory) {
        return students[_id];
        }

}
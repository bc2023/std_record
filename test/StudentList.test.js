// //Import the studentList smart contract
// const StudentList = artifacts.require('StudentList')

// //use the contract to write alll test
// //varuable: account => all accounts in blockchain
// contract('StudentList', (account) => {

//     //Make sure contract is deployed and before we retirve the studentlist object for testing
//     beforeEach(async () => {
//         this.studentList = await StudentList.deployed()
//     })

//     //Testing the deployed student contract
//     it('deploys successfully', async() => {
//         //Gte the address which the student object is stored
//         const address = await this.studentList.address
//         //Test for valid address
//         isValidAddress(address)
//     })
//     //Testing the content in the contract
// it('test adding students', async() => {
//     return StudentList.deployed().then ((instance)=>{
//         s=instance;
//         studentCid= 1;
//         return s.createStudent(studentCid, "Ugyen Lhundup")
//     }).then((transaction)=>{
//         isValidAddress(transaction.tx)
//         isValidAddress(transaction.reciept.blockHash)
//         return s.studentsCount()
//     }).then((count)=> {
//         assert.equal(count,2)
//         return s.students(2);
//     }).then((student)=> {
//         assert.equal(student.cid,studentCid)
//     })
// })

// })

// //this function check id the address is valid
// function isValidAddress(address){
//     assert.notEqual(address, 0x0)
//     assert.notEqual(address,'')
//     assert.notEqual(address,null)
//     assert.notEqual(address, undefined)
// }

//Import the StudentList smart contract
const StudentList = artifacts.require('StudentList')

//use the contract to write alll test
//varuable: account => all accounts in blockchain
contract('StudentList', (account) => {
    //Make sure contract is deployed and before we retirve the studentlist obkect for testing
    beforeEach(async () => {     
        this.studentList = await StudentList.deployed()     //it will retive the instant of deployed contract
    })

    //Testing the deployed student contract
    it('deploys successfully', async() => {                //"it" one of the funciton of mocha testing function -- used to defined new test cases
        //Get the address which the student object is stored      // "=>" arrow function = short hand for writing function in js
    
        const address = await this.studentList.address     
        //Test for valid address
        isValidAddress(address)
    })

        //testing the content in the contract
    it('test adding students', async() => {
        return StudentList.deployed().then ((instance)=> {
        s = instance;
        studentCid = 1;
        return s.createStudent(studentCid, "choki lhamo");
        
        }).then ((transaction) => {         // then- tells to exectute the remaining fucntion
        isValidAddress(transaction.tx)
        isValidAddress(transaction.receipt.blockHash);
        return s.studentsCount()

        }).then ((count) => {
        assert.equal(count, 2)
        return s.students(2);

        }).then ((student) => {
        assert.equal(studentCid, studentCid)
        })
    })
    it('test finding students', async() => {
        return StudentList.deployed().then(async(instance)=> {
            s=instance;
            studentCid=2; //is alrady taken by another student
            return s.createStudent(studentCid++,"Tshering yangzom").then(async(tx)=>{
                return s.createStudent(studentCid++,"Dorji Tshering").then(async(tx)=>{
                    return s.createStudent(studentCid++, "Chimmi Dema").then(async(tx)=>{
                        return s.createStudent(studentCid++, "Tashi Phuntsho").then(async(tx)=>{
                            return s.createStudent(studentCid++,"Samten Zangmo").then(async(tx)=>{
                                return s.createStudent(studentCid++,"Sonam Chophel").then(async(tx)=>{
                                    return s.studentsCount().then(async(count)=> {
                                        assert.equal(count, 8)
                                        return s.findStudent(5).then(async(student)=>{
                                            assert.equal(student.name, "Chimmi Dema")
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })

    //testing changing content in the contract
    it('test mark graduate students', async()=>{
        return StudentList.deployed().then(async(instance)=>{
            s=instance;
            return s.findStudent(1).then(async(ostudent)=>{
            assert.equal(ostudent.name,"Tshewang Penjor Wangchuk")
            assert.equal(ostudent.graduated, false)
            return s.markGraduated(1).then(async(transaction)=>{
                return s.findStudent(1).then(async (nstudent)=>{
                    assert.equal(nstudent.name, "Tshewang Penjor Wangchuk")
                    assert.equal(nstudent.graduated,true)
                    return
                })
            })
            })
        })
    })
})


//this function check id the address is valid
function isValidAddress(address){
    assert.notEqual(address, 0x0)       // check if values are not equal -- notEqual have 2 parameter
    assert.notEqual(address,'')
    assert.notEqual(address,null)
    assert.notEqual(address, undefined)
}


var StudentList = artifacts.require("./StudentList.sol");
var SubjectList = artifacts.require("./SubjectList.sol");

module.exports = function(deployer){
    deployer.deploy(StudentList);
    deployer.deploy(SubjectList);
}